#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/sha.h>


#define HS 32

unsigned char hash1[HS]={99,27,154,138,53,61,181,43,178,103,27,232,132,178,4,28,133,154,85,209,110,109,164,222,54,187,31,177,253,80,245,11};
unsigned char hash2[HS]={229,245,177,27,190,97,124,233,8,243,83,240,14,126,250,242,18,145,85,36,202,35,92,72,237,9,28,153,123,55,253,164};
unsigned char hash3[HS]={14,63,208,2,84,251,207,34,204,83,177,93,182,34,91,230,59,3,181,79,78,56,0,128,234,181,221,132,219,94,40,1};
unsigned char hash4[HS]={106,76,233,133,75,101,137,140,40,158,113,14,229,79,187,203,104,34,90,25,238,233,219,33,17,169,126,2,127,212,70,158};
unsigned char hash5[HS]={204,82,170,71,151,42,43,203,171,255,86,120,21,187,141,182,250,163,78,230,231,172,14,80,113,59,87,212,133,172,76,148};
unsigned char hash6[HS]={77,164,88,120,143,161,91,139,204,142,176,218,67,31,210,48,45,205,25,200,202,124,14,206,21,2,78,100,70,41,225,171};

int t = 0;

void affiche_hash_hex(unsigned char* md, int size){

    int i;
    for(i=0;i<size;i++){
        printf("%02x ",md[i]);
    }
    printf("\n");

}

void affiche_hash(unsigned char* md, int size){

    int i;
    for(i=0;i<size;i++){
        printf("%d ",md[i]);
    }
    printf("\n");

}

void hash_compare(char * hache, char * line)
{

    if (memcmp(hash1, hache, HS) == 0)
    {
        printf("Password for Hash1 found : %s\n", line);
    }
    if (memcmp(hash2, hache, HS) == 0)
    {
        printf("Password for Hash2 found : %s\n", line);
    }
    if (memcmp(hash3, hache, HS) == 0)
    {
        printf("Password for Hash3 found : %s\n", line);
    }
    if (memcmp(hash4, hache, HS) == 0)
    {
        printf("Password for Hash4 found : %s\n", line);
    }
    if (memcmp(hash5, hache, HS) == 0)
    {
        printf("Password for Hash5 found : %s\n", line);
    }
    if (memcmp(hash6, hache, HS) == 0)
    {
        printf("Password for Hash6 found : %s\n", line);
    }
}

void lineTransform(char * hache, char * line, size_t read)
{

    char save[read-1];
    char chaine1[read];
    char chaineDouble[(read-1)*2];

    char chaine1Double[read*2];

    line[strlen(line)-1] = '\0';

    strcpy(save, line);

    SHA256(line, read-1, hache);
    hash_compare(hache, line);
    if (t <= 4) printf("line : %s\n", line);

    line[0] = line[0] - 32; // T1
    SHA256(line, read-1, hache);
    hash_compare(hache, line);
    if (t <= 4) printf("line : %s\n", line);
    strcpy(line, save);

    line[strlen(line)-1] = line[strlen(line)-1] - 32; //T2
    SHA256(line, read-1, hache);
    hash_compare(hache, line);
    if (t <= 4) printf("line : %s\n", line);
    strcpy(line, save);

    strncpy(chaine1, save, read-1);
    chaine1[read-1] = 49;
    chaine1[read] = '\0'; //T3
    SHA256(chaine1, read, hache);
    hash_compare(hache, chaine1);
    if (t <= 4) printf("line : %s\n", chaine1);
    strcpy(line, save);

    strcpy(chaineDouble, line);
    strcat(chaineDouble, line);  // T4
    SHA256(chaineDouble, (read-1)*2, hache); 
    hash_compare(hache, chaineDouble);
    if (t <= 4) printf("line : %s\n", chaineDouble);
    strcpy(line, save);

    line[0] = line[0] - 32; // T1
    strcpy(chaineDouble, line);
    strcat(chaineDouble, line);  // T4
    SHA256(chaineDouble, (read-1)*2, hache); 
    hash_compare(hache, chaineDouble);
    if (t <= 4) printf("line : %s\n", chaineDouble);
    strcpy(line, save);

    if (t <= 4) printf("--------------\n");

    line[0] = line[0] - 32; // T1
    strncpy(chaine1, line, read-1);
    strcat(chaine1, "1");//T3
    strcpy(chaine1Double, chaine1);
    strcat(chaine1Double, line);  // T4
    chaine1Double[strlen(chaine1Double)-1] = chaine1Double[strlen(chaine1Double)-1] - 32; //T2
    SHA256(chaine1Double, read*2, hache); 
    hash_compare(hache, chaine1Double);
    if (t <= 4) printf("line : %s\n", chaine1Double);

    if (t <= 4) printf("--------------\n");

    t++;
}

int main(int argc, char ** argv)
{
    char hache[HS];

    FILE * file = NULL;
    char * filename = NULL;

    char * line = NULL;
    size_t len = 0;
    size_t read;

    if(argc != 2)
    {
        printf("Error, this program expect one param");
        exit(EXIT_FAILURE);
    }

    filename = (char *) malloc(sizeof(char) * strlen(argv[1])+1);
    if(filename == NULL)
    {
        printf("Error malloc !");
        exit(EXIT_FAILURE);
    }

    strcpy(filename, argv[1]);

    printf("filename : %s\n", filename);

    file = fopen(filename, "r");
    if (file == NULL)
    {
        printf("Error fopen !");
        exit(EXIT_FAILURE);
    }

    while ((read = getline(&line, &len, file)) != -1) {
        lineTransform(hache, line, read);
    }

    fclose(file);
    free(filename);

    return 0;
}
