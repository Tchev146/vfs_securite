#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/sha.h>


#define HS 20 //taille en octets du hash pour SHA1

void affiche_hash_hex(unsigned char* md, int size){
    int i;
    for(i=0;i<size;i++){
        printf("%02x ",md[i]);
    }
    printf("\n");

}

void affiche_hash(unsigned char* md, int size){
    int i;
    for(i=0;i<size;i++){
        printf("%d ",md[i]);
    }
    printf("\n");

}

int main(int argc, char ** argv)
{
    SHA_CTX ctx;
    unsigned char md[HS];
    char exemple[5]="test";
    unsigned char buf[8192];

    FILE * file = NULL;
    char * filename = NULL;

    if(argc != 2)
    {
        printf("Error, this program expect one param");
        exit(EXIT_FAILURE);
    }

    filename = (char *) malloc(sizeof(char) * strlen(argv[1])+1);
    if(filename == NULL)
    {
        printf("Error malloc !");
        exit(EXIT_FAILURE);
    }

    strcpy(filename, argv[1]);

    printf("filename : %s\n", filename);

    file = fopen(filename, "r");
    if (file == NULL)
    {
        printf("Error fopen !");
        exit(EXIT_FAILURE);
    }
    

    if(SHA1_Init(&ctx) == 0){
        printf("Erreur Init\n");
        return 1;
    }
/*
    if(SHA1_Update(&ctx,exemple,4) == 0){ //4 peut ici être remplacé par strlen(exemple)
        printf("Erreur Update\n");
        return 1;
    }
*/

    for (;;) {
        size_t len;

        len = fread(buf, 1, sizeof buf, file);
        if (len == 0)
            break;
        SHA1_Update(&ctx, buf, len);
    }

    if(SHA1_Final(md,&ctx)== 0){
        printf("Erreur Final\n");
        return 1;
    }


    affiche_hash(md,HS);
    affiche_hash_hex(md,HS);

    fclose(file);
    free(filename);

    return 0;
}
