#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/sha.h>

#define HS 32

void affiche_hash_hex(unsigned char* md, int size){

    int i;
    for(i=0;i<size;i++){
        printf("%02x ",md[i]);
    }
    printf("\n");

}

void affiche_hash(unsigned char* md, int size){

    int i;
    for(i=0;i<size;i++){
        printf("%d ",md[i]);
    }
    printf("\n");

}

int main(int argc, char ** argv)
{
    unsigned char hash1[HS]={236,206,10,240,218,213,228,12,63,20,93,130,211,252,50,177,182,100,85,191,151,152,181,243,245,3,5,246,127,236,44,1};
    unsigned char hash2[HS]={135,16,231,12,145,125,162,28,145,157,139,15,31,82,56,219,5,35,203,187,210,89,57,19,85,166,145,219,105,86,146,210};

    char hache[HS];

    FILE * file = NULL;
    char * filename = NULL;

    char * line = NULL;
    size_t len = 0;
    size_t read;

    if(argc != 2)
    {
        printf("Error, this program expect one param");
        exit(EXIT_FAILURE);
    }

    filename = (char *) malloc(sizeof(char) * strlen(argv[1])+1);
    if(filename == NULL)
    {
        printf("Error malloc !");
        exit(EXIT_FAILURE);
    }

    strcpy(filename, argv[1]);

    printf("filename : %s\n", filename);

    file = fopen(filename, "r");
    if (file == NULL)
    {
        printf("Error fopen !");
        exit(EXIT_FAILURE);
    }

    while ((read = getline(&line, &len, file)) != -1) {
        SHA256(line, read-1, hache);
        if (memcmp(hash1, hache, HS) == 0)
        {
            printf("Password for Hash1 found : %s\n", line);
        }
        if (memcmp(hash2, hache, HS) == 0)
        {
            printf("Password for Hash1 found : %s\n", line);
        }
    }

    fclose(file);
    free(filename);

    return 0;
}
